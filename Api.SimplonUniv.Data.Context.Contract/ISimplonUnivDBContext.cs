﻿using Api.SimplonUniv.Data.Entity.Model;
using Microsoft.EntityFrameworkCore;
using Registry = Api.SimplonUniv.Data.Entity.Model.Registry;

namespace Api.SimplonUniv.Data.Context.Contract
{
    public interface ISimplonUnivDBContext : IDbContext
    {
        DbSet<Category> Categories { get; set; }
        DbSet<Course> Courses { get; set; }
        DbSet<Cycle> Cycles { get; set; }
        DbSet<Exam> Exams { get; set; }
        DbSet<Level> Levels { get; set; }
        DbSet<Mark> Marks { get; set; }
        DbSet<Professor> Professors { get; set; }
        DbSet<Registry> Registries { get; set; }
        DbSet<Room> Rooms { get; set; }
        DbSet<Session> Sessions { get; set; }
        DbSet<Student> Students { get; set; }
        DbSet<Studentlevel> Studentlevels { get; set; }
        DbSet<Subject> Subjects { get; set; }
    }
}