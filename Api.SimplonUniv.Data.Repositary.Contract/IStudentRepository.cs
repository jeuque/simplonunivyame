﻿using Api.SimplonUniv.Data.Entity.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.SimplonUniv.Data.Repository.Contract
{
    public interface IStudentRepository : IGenericRepository<Student>
    {

    }
}
