﻿using Api.SimplonUniv.Business.Services.Contract;
using Api.SimplonUniv.Data.Repository.Contract;

namespace Api.SimplonUniv.Business.Services
{
    public class RegistryService : IRegistryService
    {
        private readonly IRegistryRepository _registryRepository;

        public RegistryService(IRegistryRepository registryRepository)
        {
            _registryRepository = registryRepository;
        }
    }
}