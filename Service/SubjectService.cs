﻿using Api.SimplonUniv.Business.Services.Contract;
using Api.SimplonUniv.Data.Repository.Contract;

namespace Api.SimplonUniv.Business.Services
{
    public class SubjectService : ISubjectService
    {
        private readonly ISubjectRepository _subjectRepository;

        public SubjectService(ISubjectRepository subjectRepository)
        {
            _subjectRepository = subjectRepository;
        }
    }
}