﻿using Api.SimplonUniv.Business.Services.Contract;
using Api.SimplonUniv.Data.Repository.Contract;

namespace Api.SimplonUniv.Business.Services
{
    public class StudentService : IStudentService
    {
        private readonly IStudentRepository _studentRepository;

        public StudentService(IStudentRepository studentRepository)
        {
            _studentRepository = studentRepository;
        }
    }
}