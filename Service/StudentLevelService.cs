﻿using Api.SimplonUniv.Business.Services.Contract;
using Api.SimplonUniv.Data.Repository.Contract;

namespace Api.SimplonUniv.Business.Services
{
    public class StudentLevelService : IStudentLevelService
    {
        private readonly IStudentLevelRepository _studentLevelRepository;

        public StudentLevelService(IStudentLevelRepository studentLevelRepository)
        {
            _studentLevelRepository = studentLevelRepository;
        }
    }
}