﻿using Api.SimplonUniv.Business.Services.Contract;
using Api.SimplonUniv.Data.Repository.Contract;

namespace Api.SimplonUniv.Business.Service
{
    public class ProfessorService : IProfessorService
    {
        private readonly IProfessorRepository _professorRepository;

        public ProfessorService(IProfessorRepository professorRepository)
        {
            _professorRepository = professorRepository;
        }
    }
}