﻿using Api.SimplonUniv.Business.Services.Contract;
using Api.SimplonUniv.Data.Repository.Contract;

namespace Api.SimplonUniv.Business.Service
{
    public class CycleService : ICycleService
    {
        private readonly ICycleRepository _cycleRepository;

        public CycleService(ICycleRepository cycleRepository)
        {
            _cycleRepository = cycleRepository;
        }
    }
}