﻿using Api.SimplonUniv.Business.Services.Contract;
using Api.SimplonUniv.Data.Repository.Contract;

namespace Api.SimplonUniv.Business.Services
{
    public class SessionService : ISessionService
    {
        private readonly ISessionRepository _sessionRepository;

        public SessionService(ISessionRepository sessionRepository)
        {
            _sessionRepository = sessionRepository;
        }
    }
}