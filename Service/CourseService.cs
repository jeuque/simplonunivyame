﻿using Api.SimplonUniv.Data.Entity.Model;
using Api.SimplonUniv.Data.Repository.Contract;
using Api.SimplonUniv.Business.Services.Contract;

namespace Api.SimplonUniv.Business.Service
{
    public class  CourseService : ICourseService
    {
        private readonly ICourseRepository _courseRepository;

        public CourseService(ICourseRepository courseRepository)
        {
            _courseRepository = courseRepository;
        }
    }
}