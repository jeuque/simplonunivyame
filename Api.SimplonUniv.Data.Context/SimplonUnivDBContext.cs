﻿using Api.SimplonUniv.Data.Context.Contract;
using Api.SimplonUniv.Data.Entity.Model;
using Microsoft.EntityFrameworkCore;

namespace Api.SimplonUniv.Data.Entity
{
    public partial class SimplonUnivDBContext : DbContext, ISimplonUnivDBContext
    {
        public SimplonUnivDBContext()
        {
        }

        public SimplonUnivDBContext(DbContextOptions<SimplonUnivDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Category> Categories { get; set; } = null!;
        public virtual DbSet<Course> Courses { get; set; } = null!;
        public virtual DbSet<Cycle> Cycles { get; set; } = null!;
        public virtual DbSet<Exam> Exams { get; set; } = null!;
        public virtual DbSet<Level> Levels { get; set; } = null!;
        public virtual DbSet<Mark> Marks { get; set; } = null!;
        public virtual DbSet<Professor> Professors { get; set; } = null!;
        public virtual DbSet<Registry> Registries { get; set; } = null!;
        public virtual DbSet<Room> Rooms { get; set; } = null!;
        public virtual DbSet<Session> Sessions { get; set; } = null!;
        public virtual DbSet<Student> Students { get; set; } = null!;
        public virtual DbSet<Studentlevel> Studentlevels { get; set; } = null!;
        public virtual DbSet<Subject> Subjects { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
               optionsBuilder.UseMySql("Server=localhost;User=root;Database=simplongestion;port=3306", Microsoft.EntityFrameworkCore.ServerVersion.Parse("8.0.30-mysql"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.UseCollation("utf8mb4_0900_ai_ci")
                .HasCharSet("utf8mb4");

            modelBuilder.Entity<Category>(entity =>
            {
                entity.ToTable("category");

                entity.Property(e => e.CategoryId).HasColumnName("Category_id");

                entity.Property(e => e.CategoryTitle)
                    .HasMaxLength(64)
                    .HasColumnName("Category_title");
            });

            modelBuilder.Entity<Course>(entity =>
            {
                entity.ToTable("course");

                entity.HasIndex(e => e.CycleId, "Cycle_id");

                entity.HasIndex(e => e.SubjectId, "Subject_id");

                entity.Property(e => e.CourseId).HasColumnName("Course_id");

                entity.Property(e => e.CycleId).HasColumnName("Cycle_id");

                entity.Property(e => e.SubjectId).HasColumnName("Subject_id");

                entity.HasOne(d => d.Cycle)
                    .WithMany(p => p.Courses)
                    .HasForeignKey(d => d.CycleId)
                    .HasConstraintName("course_ibfk_1");

                entity.HasOne(d => d.Subject)
                    .WithMany(p => p.Courses)
                    .HasForeignKey(d => d.SubjectId)
                    .HasConstraintName("course_ibfk_2");
            });

            modelBuilder.Entity<Cycle>(entity =>
            {
                entity.ToTable("cycle");

                entity.Property(e => e.CycleId).HasColumnName("Cycle_id");

                entity.Property(e => e.CycleEndingDate)
                    .HasColumnType("datetime")
                    .HasColumnName("Cycle_endingDate");

                entity.Property(e => e.CycleStartingDate)
                    .HasColumnType("datetime")
                    .HasColumnName("Cycle_startingDate");
            });

            modelBuilder.Entity<Exam>(entity =>
            {
                entity.ToTable("exam");

                entity.HasIndex(e => e.CourseId, "course_id");

                entity.Property(e => e.ExamId).HasColumnName("exam_id");

                entity.Property(e => e.CourseId).HasColumnName("course_id");

                entity.Property(e => e.ExamType)
                    .HasMaxLength(64)
                    .HasColumnName("exam_type");

                entity.HasOne(d => d.Course)
                    .WithMany(p => p.Exams)
                    .HasForeignKey(d => d.CourseId)
                    .HasConstraintName("exam_ibfk_1");
            });

            modelBuilder.Entity<Level>(entity =>
            {
                entity.ToTable("level");

                entity.Property(e => e.LevelId).HasColumnName("level_id");

                entity.Property(e => e.LevelTitle)
                    .HasMaxLength(255)
                    .HasColumnName("level_title");
            });

            modelBuilder.Entity<Mark>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("mark");

                entity.HasIndex(e => e.ExamId, "exam_id");

                entity.HasIndex(e => e.StudentId, "student_id");

                entity.Property(e => e.ExamId).HasColumnName("exam_id");

                entity.Property(e => e.MarkValue).HasColumnName("mark_value");

                entity.Property(e => e.StudentId).HasColumnName("student_id");

                entity.HasOne(d => d.Exam)
                    .WithMany()
                    .HasForeignKey(d => d.ExamId)
                    .HasConstraintName("mark_ibfk_2");

                entity.HasOne(d => d.Student)
                    .WithMany()
                    .HasForeignKey(d => d.StudentId)
                    .HasConstraintName("mark_ibfk_1");
            });

            modelBuilder.Entity<Professor>(entity =>
            {
                entity.ToTable("professor");

                entity.Property(e => e.ProfessorId).HasColumnName("Professor_id");

                entity.Property(e => e.ProfessorEmail)
                    .HasMaxLength(255)
                    .HasColumnName("Professor_email");

                entity.Property(e => e.ProfessorFirstname)
                    .HasMaxLength(64)
                    .HasColumnName("Professor_firstname");

                entity.Property(e => e.ProfessorName)
                    .HasMaxLength(64)
                    .HasColumnName("Professor_name");

                entity.HasMany(d => d.Courses)
                    .WithMany(p => p.Professors)
                    .UsingEntity<Dictionary<string, object>>(
                        "Professorcourse",
                        l => l.HasOne<Course>().WithMany().HasForeignKey("CourseId").OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("professorcourse_ibfk_2"),
                        r => r.HasOne<Professor>().WithMany().HasForeignKey("ProfessorId").OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("professorcourse_ibfk_1"),
                        j =>
                        {
                            j.HasKey("ProfessorId", "CourseId").HasName("PRIMARY").HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                            j.ToTable("professorcourse");

                            j.HasIndex(new[] { "CourseId" }, "Course_id");

                            j.IndexerProperty<int>("ProfessorId").HasColumnName("Professor_id");

                            j.IndexerProperty<int>("CourseId").HasColumnName("Course_id");
                        });

                entity.HasMany(d => d.Subjects)
                    .WithMany(p => p.Professors)
                    .UsingEntity<Dictionary<string, object>>(
                        "Professorsubject",
                        l => l.HasOne<Subject>().WithMany().HasForeignKey("SubjectId").OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("professorsubject_ibfk_2"),
                        r => r.HasOne<Professor>().WithMany().HasForeignKey("ProfessorId").OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("professorsubject_ibfk_1"),
                        j =>
                        {
                            j.HasKey("ProfessorId", "SubjectId").HasName("PRIMARY").HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                            j.ToTable("professorsubject");

                            j.HasIndex(new[] { "SubjectId" }, "Subject_id");

                            j.IndexerProperty<int>("ProfessorId").HasColumnName("Professor_id");

                            j.IndexerProperty<int>("SubjectId").HasColumnName("Subject_id");
                        });
            });

            modelBuilder.Entity<Registry>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("registry");

                entity.HasIndex(e => e.CourseId, "course_id");

                entity.HasIndex(e => e.StudentId, "student_id");

                entity.Property(e => e.CourseId).HasColumnName("course_id");

                entity.Property(e => e.RegistryAnnulation)
                    .HasColumnType("datetime")
                    .HasColumnName("registry_annulation");

                entity.Property(e => e.RegistryAnnulationMotif)
                    .HasMaxLength(255)
                    .HasColumnName("registry_annulationMotif");

                entity.Property(e => e.RegistryDate)
                    .HasColumnType("datetime")
                    .HasColumnName("registry_date");

                entity.Property(e => e.StudentId).HasColumnName("student_id");

                entity.HasOne(d => d.Course)
                    .WithMany()
                    .HasForeignKey(d => d.CourseId)
                    .HasConstraintName("registry_ibfk_2");

                entity.HasOne(d => d.Student)
                    .WithMany()
                    .HasForeignKey(d => d.StudentId)
                    .HasConstraintName("registry_ibfk_1");
            });

            modelBuilder.Entity<Room>(entity =>
            {
                entity.ToTable("room");

                entity.Property(e => e.RoomId).HasColumnName("Room_id");

                entity.Property(e => e.RoomName)
                    .HasMaxLength(64)
                    .HasColumnName("Room_name");

                entity.Property(e => e.RoomPlaces).HasColumnName("Room_places");
            });

            modelBuilder.Entity<Session>(entity =>
            {
                entity.ToTable("session");

                entity.HasIndex(e => e.CourseId, "Course_id");

                entity.HasIndex(e => e.ProfessorId, "Professor_id");

                entity.HasIndex(e => e.RoomId, "Room_id");

                entity.Property(e => e.SessionId).HasColumnName("Session_id");

                entity.Property(e => e.CourseId).HasColumnName("Course_id");

                entity.Property(e => e.ProfessorId).HasColumnName("Professor_id");

                entity.Property(e => e.RoomId).HasColumnName("Room_id");

                entity.Property(e => e.SessionEndingDate)
                    .HasColumnType("datetime")
                    .HasColumnName("Session_endingDate");

                entity.Property(e => e.SessionStartingDate)
                    .HasColumnType("datetime")
                    .HasColumnName("Session_startingDate");

                entity.HasOne(d => d.Course)
                    .WithMany(p => p.Sessions)
                    .HasForeignKey(d => d.CourseId)
                    .HasConstraintName("session_ibfk_1");

                entity.HasOne(d => d.Professor)
                    .WithMany(p => p.Sessions)
                    .HasForeignKey(d => d.ProfessorId)
                    .HasConstraintName("session_ibfk_2");

                entity.HasOne(d => d.Room)
                    .WithMany(p => p.Sessions)
                    .HasForeignKey(d => d.RoomId)
                    .HasConstraintName("session_ibfk_3");
            });

            modelBuilder.Entity<Student>(entity =>
            {
                entity.ToTable("student");

                entity.Property(e => e.StudentId).HasColumnName("student_id");

                entity.Property(e => e.StudentAdressCity)
                    .HasMaxLength(255)
                    .HasColumnName("student_adress_city");

                entity.Property(e => e.StudentAdressCountry)
                    .HasMaxLength(255)
                    .HasColumnName("student_adress_country");

                entity.Property(e => e.StudentAdressName)
                    .HasMaxLength(255)
                    .HasColumnName("student_adress_name");

                entity.Property(e => e.StudentAdressZipcode)
                    .HasMaxLength(10)
                    .HasColumnName("student_adress_zipcode");

                entity.Property(e => e.StudentBirthDate).HasColumnName("student_birthDate");

                entity.Property(e => e.StudentEmail)
                    .HasMaxLength(255)
                    .HasColumnName("student_email");

                entity.Property(e => e.StudentFirstname)
                    .HasMaxLength(64)
                    .HasColumnName("student_firstname");

                entity.Property(e => e.StudentJoiningDate).HasColumnName("student_joiningDate");

                entity.Property(e => e.StudentName)
                    .HasMaxLength(64)
                    .HasColumnName("student_name");

                entity.Property(e => e.StudentPhone)
                    .HasMaxLength(15)
                    .HasColumnName("student_phone");

                entity.HasMany(d => d.Sessions)
                    .WithMany(p => p.Students)
                    .UsingEntity<Dictionary<string, object>>(
                        "Presence",
                        l => l.HasOne<Session>().WithMany().HasForeignKey("SessionId").OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK_Session_Presence"),
                        r => r.HasOne<Student>().WithMany().HasForeignKey("StudentId").OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK_Student_Presence"),
                        j =>
                        {
                            j.HasKey("StudentId", "SessionId").HasName("PRIMARY").HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                            j.ToTable("presence");

                            j.HasIndex(new[] { "SessionId" }, "FK_Session_Presence");

                            j.IndexerProperty<int>("StudentId").HasColumnName("student_id");

                            j.IndexerProperty<int>("SessionId").HasColumnName("session_id");
                        });
            });

            modelBuilder.Entity<Studentlevel>(entity =>
            {
                entity.HasKey(e => new { e.LevelId, e.StudentId })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("studentlevel");

                entity.HasIndex(e => e.StudentId, "FK_Student_LinkStudentlevel");

                entity.Property(e => e.LevelId).HasColumnName("level_id");

                entity.Property(e => e.StudentId).HasColumnName("student_id");

                entity.Property(e => e.StudentLevelValidated).HasColumnName("StudentLevel_Validated");

                entity.Property(e => e.StudentLevelYear)
                    .HasMaxLength(64)
                    .HasColumnName("studentLevel_year");

                entity.HasOne(d => d.Level)
                    .WithMany(p => p.Studentlevels)
                    .HasForeignKey(d => d.LevelId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Level_LinkStudentlevel");

                entity.HasOne(d => d.Student)
                    .WithMany(p => p.Studentlevels)
                    .HasForeignKey(d => d.StudentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Student_LinkStudentlevel");
            });

            modelBuilder.Entity<Subject>(entity =>
            {
                entity.ToTable("subject");

                entity.HasIndex(e => e.CategoryId, "Category_id");

                entity.Property(e => e.SubjectId).HasColumnName("Subject_id");

                entity.Property(e => e.CategoryId).HasColumnName("Category_id");

                entity.Property(e => e.SubjectDescription)
                    .HasMaxLength(255)
                    .HasColumnName("Subject_description");

                entity.Property(e => e.SubjectTitle)
                    .HasMaxLength(64)
                    .HasColumnName("Subject_title");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Subjects)
                    .HasForeignKey(d => d.CategoryId)
                    .HasConstraintName("subject_ibfk_1");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}