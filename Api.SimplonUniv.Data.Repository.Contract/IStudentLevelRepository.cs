﻿using Api.SimplonUniv.Data.Entity.Model;

namespace Api.SimplonUniv.Data.Repository.Contract
{
    public interface IStudentLevelRepository : IGenericRepository<Studentlevel>
    {
    }
}