using Api.SimplonUniv.Data.Entity.Model;

namespace Api.SimplonUniv.Data.Repository.Contract
{
    public interface IProfessorRepository : IGenericRepository<Professor>
    {
    }
}