﻿using Api.SimplonUniv.Data.Entity.Model;

namespace Api.SimplonUniv.Data.Repository.Contract
{
    public interface ISessionRepository : IGenericRepository<Session>
    {
    }
}