﻿using Api.SimplonUniv.Data.Entity.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.SimplonUniv.Business.Dto.Student
{
    public class CreateStudentDto
    {
        public string StudentName { get; set; }
        public string StudentFirstname { get; set; }
        public string StudentEmail { get; set; }
        public DateOnly StudentBirthDate { get; set; }
        public string StudentPhone { get; set; }
        public string StudentAdressName { get; set; }
        public string StudentAdressCity { get; set; }
        public string StudentAdressZipcode { get; set; }
        public string StudentAdressCountry { get; set; }
    }
    public class GetStudentBaseDto
    {
        public string StudentName { get; set; }
        public string StudentFirstname { get; set; }
    }
    public class GetStudentDto
    {
        public string StudentName { get; set; }
        public string StudentFirstname { get; set; }
        public string StudentEmail { get; set; }
        public DateOnly StudentBirthDate { get; set; }
        public string StudentPhone { get; set; }
        public string StudentAdressName { get; set; }
        public string StudentAdressCity { get; set; }
        public string StudentAdressZipcode { get; set; }
        public string StudentAdressCountry { get; set; }
    }
    public class ReadStudentDto
    {
        public int StudentId { get; set; }
    }
}
