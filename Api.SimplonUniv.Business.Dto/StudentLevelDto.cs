﻿using Api.SimplonUniv.Data.Entity.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.SimplonUniv.Business.Dto
{
    public class CreateStudentLevelDto
    {
        public int LevelId { get; set; }
        public int StudentId { get; set; }
        public string StudentLevelYear { get; set; }
        public bool? StudentLevelValidated { get; set; }
    }
    public class GetStudentLevelDto
    {
        public int LevelId { get; set; }
        public int StudentId { get; set; }
        public string StudentLevelYear { get; set; }
        public bool? StudentLevelValidated { get; set; }
    }
    public class ReadStudentLevel
    {
        public int LevelId { get; set; }
        public int StudentId { get; set; }
    }
}
