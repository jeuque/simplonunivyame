﻿using Api.SimplonUniv.Data.Entity.Model;

namespace Api.SimplonUniv.Business.Dto.Professor
{
    public class CreateProfessorDto
    {
        public int ProfessorId { get; set; }
        public string ProfessorFirstname { get; set; }
        public string ProfessorName { get; set; }
        public string ProfessorEmail { get; set; }
    }

    public class GetProfessorDto
    {
        public string ProfessorFirstname { get; set; }
        public string ProfessorName { get; set; }
        public string ProfessorEmail { get; set; }
    }

    public class ReadProfessorDto
    {
        public int ProfessorId { get; set; }
    }
}