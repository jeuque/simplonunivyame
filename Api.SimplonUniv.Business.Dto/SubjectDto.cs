﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.SimplonUniv.Business.Dto
{
    public class CreateSubjectDto
    {
        public string SubjectTitle { get; set; }
        public string SubjectDescription { get; set; }
        public int CategoryId { get; set; }
    }
    public class GetSubjectDto
    {
        public string SubjectTitle { get; set; }
        public string SubjectDescription { get; set; }
        public int CategoryId { get; set; }
    }

    public class ReadSubjectDto
    {
        public int SubjectId { get; set; }
    }
}
