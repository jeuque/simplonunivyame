using Api.SimplonUniv.Data.Context.Contract;
using Api.SimplonUniv.Data.Entity.Model;
using Api.SimplonUniv.Data.Repository.Contract;
using Microsoft.EntityFrameworkCore;

namespace Api.SimplonUniv.Data.Repository
{
    public abstract class RoomRepository : GenericRepository<Room>, IRoomRepository
    {
        public RoomRepository(ISimplonUnivDBContext simplonUnivDBContext) : base(simplonUnivDBContext)
        {
        }

        public async Task<Room> GetRoomByNameAsync(string roomName)
        {
            return await _simplonUnivDbContext.Rooms.FirstOrDefaultAsync(x => x.RoomName == roomName).ConfigureAwait(false);
        }
    }
}