using Api.SimplonUniv.Data.Context.Contract;
using Api.SimplonUniv.Data.Entity.Model;
using Api.SimplonUniv.Data.Repository.Contract;

namespace Api.SimplonUniv.Data.Repository
{
    public abstract class ProfessorRepository : GenericRepository<Professor>, IProfessorRepository
    {
        public ProfessorRepository(ISimplonUnivDBContext simplonUnivDbContext) : base(simplonUnivDbContext)
        {
        }
    }
}