﻿using Api.SimplonUniv.Data.Context.Contract;
using Api.SimplonUniv.Data.Entity.Model;
using Api.SimplonUniv.Data.Repository.Contract;

namespace Api.SimplonUniv.Data.Repository
{
    public abstract class RegistryRepository : GenericRepository<Registry>, IRegistryRepository
    {
        public RegistryRepository(ISimplonUnivDBContext simplonUnivDbContext) : base(simplonUnivDbContext)
        {
        }
    }
}