﻿using Api.SimplonUniv.Data.Context.Contract;
using Api.SimplonUniv.Data.Entity.Model;
using Api.SimplonUniv.Data.Repository.Contract;

namespace Api.SimplonUniv.Data.Repository
{
    public abstract class MarkRepository : GenericRepository<Mark>, IMarkRepository
    {
        public MarkRepository(ISimplonUnivDBContext simplonUnivDbContext) : base(simplonUnivDbContext)
        {
        }
    }
}