﻿using Api.SimplonUniv.Data.Context.Contract;
using Api.SimplonUniv.Data.Entity.Model;
using Api.SimplonUniv.Data.Repository.Contract;

namespace Api.SimplonUniv.Data.Repository
{
    public abstract class StudentRepository : GenericRepository<Student>, IStudentRepository
    {
        public StudentRepository(ISimplonUnivDBContext simplonUnivDbContext) : base(simplonUnivDbContext)
        {
        }
    }
}