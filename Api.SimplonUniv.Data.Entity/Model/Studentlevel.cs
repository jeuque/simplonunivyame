﻿namespace Api.SimplonUniv.Data.Entity.Model
{
    public partial class Studentlevel
    {
        public int LevelId { get; set; }
        public int StudentId { get; set; }
        public string? StudentLevelYear { get; set; }
        public bool? StudentLevelValidated { get; set; }

        public virtual Level Level { get; set; } = null!;
        public virtual Student Student { get; set; } = null!;
    }
}