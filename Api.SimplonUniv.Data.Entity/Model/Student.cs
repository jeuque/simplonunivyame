﻿namespace Api.SimplonUniv.Data.Entity.Model
{
    public partial class Student
    {
        public Student()
        {
            Studentlevels = new HashSet<Studentlevel>();
            Sessions = new HashSet<Session>();
        }

        public int StudentId { get; set; }
        public string? StudentName { get; set; }
        public string? StudentFirstname { get; set; }
        public string? StudentEmail { get; set; }
        public DateOnly? StudentBirthDate { get; set; }
        public string? StudentPhone { get; set; }
        public DateOnly? StudentJoiningDate { get; set; }
        public string? StudentAdressName { get; set; }
        public string? StudentAdressCity { get; set; }
        public string? StudentAdressZipcode { get; set; }
        public string? StudentAdressCountry { get; set; }

        public virtual ICollection<Studentlevel> Studentlevels { get; set; }

        public virtual ICollection<Session> Sessions { get; set; }
    }
}