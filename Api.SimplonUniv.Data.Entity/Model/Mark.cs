﻿namespace Api.SimplonUniv.Data.Entity.Model
{
    public partial class Mark
    {
        public int? StudentId { get; set; }
        public int? ExamId { get; set; }
        public float? MarkValue { get; set; }

        public virtual Exam? Exam { get; set; }
        public virtual Student? Student { get; set; }
    }
}