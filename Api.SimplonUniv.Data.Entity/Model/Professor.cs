﻿namespace Api.SimplonUniv.Data.Entity.Model
{
    public partial class Professor
    {
        public Professor()
        {
            Sessions = new HashSet<Session>();
            Courses = new HashSet<Course>();
            Subjects = new HashSet<Subject>();
        }

        public int ProfessorId { get; set; }
        public string? ProfessorFirstname { get; set; }
        public string? ProfessorName { get; set; }
        public string? ProfessorEmail { get; set; }

        public virtual ICollection<Session> Sessions { get; set; }

        public virtual ICollection<Course> Courses { get; set; }
        public virtual ICollection<Subject> Subjects { get; set; }
    }
}