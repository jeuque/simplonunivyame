﻿namespace Api.SimplonUniv.Data.Entity.Model
{
    public partial class Room
    {
        public Room()
        {
            Sessions = new HashSet<Session>();
        }

        public int RoomId { get; set; }
        public string? RoomName { get; set; }
        public int? RoomPlaces { get; set; }

        public virtual ICollection<Session> Sessions { get; set; }
    }
}