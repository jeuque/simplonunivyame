﻿namespace Api.SimplonUniv.Data.Entity.Model
{
    public partial class Registry
    {
        public int? StudentId { get; set; }
        public int? CourseId { get; set; }
        public DateTime? RegistryAnnulation { get; set; }
        public DateTime? RegistryDate { get; set; }
        public string? RegistryAnnulationMotif { get; set; }

        public virtual Course? Course { get; set; }
        public virtual Student? Student { get; set; }
    }
}