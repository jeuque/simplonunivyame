﻿namespace Api.SimplonUniv.Data.Entity.Model
{
    public partial class Subject
    {
        public Subject()
        {
            Courses = new HashSet<Course>();
            Professors = new HashSet<Professor>();
        }

        public int SubjectId { get; set; }
        public string? SubjectTitle { get; set; }
        public string? SubjectDescription { get; set; }
        public int? CategoryId { get; set; }

        public virtual Category? Category { get; set; }
        public virtual ICollection<Course> Courses { get; set; }

        public virtual ICollection<Professor> Professors { get; set; }
    }
}