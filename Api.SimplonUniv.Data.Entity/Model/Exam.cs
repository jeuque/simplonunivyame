﻿namespace Api.SimplonUniv.Data.Entity.Model
{
    public partial class Exam
    {
        public int ExamId { get; set; }
        public string? ExamType { get; set; }
        public int? CourseId { get; set; }

        public virtual Course? Course { get; set; }
    }
}