﻿namespace Api.SimplonUniv.Data.Entity.Model
{
    public partial class Course
    {
        public Course()
        {
            Exams = new HashSet<Exam>();
            Sessions = new HashSet<Session>();
            Professors = new HashSet<Professor>();
        }

        public int CourseId { get; set; }
        public int? CycleId { get; set; }
        public int? SubjectId { get; set; }

        public virtual Cycle? Cycle { get; set; }
        public virtual Subject? Subject { get; set; }
        public virtual ICollection<Exam> Exams { get; set; }
        public virtual ICollection<Session> Sessions { get; set; }

        public virtual ICollection<Professor> Professors { get; set; }
    }
}