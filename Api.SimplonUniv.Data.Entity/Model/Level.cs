﻿namespace Api.SimplonUniv.Data.Entity.Model
{
    public partial class Level
    {
        public Level()
        {
            Studentlevels = new HashSet<Studentlevel>();
        }

        public int LevelId { get; set; }
        public string? LevelTitle { get; set; }

        public virtual ICollection<Studentlevel> Studentlevels { get; set; }
    }
}