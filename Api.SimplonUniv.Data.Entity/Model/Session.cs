﻿namespace Api.SimplonUniv.Data.Entity.Model
{
    public partial class Session
    {
        public Session()
        {
            Students = new HashSet<Student>();
        }

        public int SessionId { get; set; }
        public DateTime? SessionStartingDate { get; set; }
        public DateTime? SessionEndingDate { get; set; }
        public int? CourseId { get; set; }
        public int? ProfessorId { get; set; }
        public int? RoomId { get; set; }

        public virtual Course? Course { get; set; }
        public virtual Professor? Professor { get; set; }
        public virtual Room? Room { get; set; }

        public virtual ICollection<Student> Students { get; set; }
    }
}