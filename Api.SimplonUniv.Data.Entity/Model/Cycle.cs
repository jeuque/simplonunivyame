﻿namespace Api.SimplonUniv.Data.Entity.Model
{
    public partial class Cycle
    {
        public Cycle()
        {
            Courses = new HashSet<Course>();
        }

        public int CycleId { get; set; }
        public DateTime? CycleStartingDate { get; set; }
        public DateTime? CycleEndingDate { get; set; }

        public virtual ICollection<Course> Courses { get; set; }
    }
}