﻿namespace Api.SimplonUniv.Data.Entity.Model
{
    public partial class Category
    {
        public Category()
        {
            Subjects = new HashSet<Subject>();
        }

        public int CategoryId { get; set; }
        public string? CategoryTitle { get; set; }

        public virtual ICollection<Subject> Subjects { get; set; }
    }
}